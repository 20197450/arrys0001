﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio9
{
    class Program
    {
        static void Main(string[] args)
        {
            /*9 Crear una clase que permita ingresar valores
            enteros por teclado y nos muestre la tabla de
            multiplicar de dicho valor. Finalizar el programa al ingresar el -1.*/


            Console.WriteLine("          Bienvenido, este programa despliega la tabla del valor ingresado.");
            Console.WriteLine("////////////////////////////////////////////////////////////////////////////////////");

            int valoringresado, multiplicacion = 1;
            do
            {
                Console.Write("Ingrese un valor: ");
                valoringresado = int.Parse(Console.ReadLine());
                for (int i = 0; i < 11; i++)
                {
                    multiplicacion = i * valoringresado;
                    Console.WriteLine("{0} x {1} = {2}", valoringresado, i, multiplicacion);
                }
            } while (valoringresado != -1);



            Console.WriteLine("FIN");
            Console.ReadKey();
        }
       
    }


}
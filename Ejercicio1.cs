﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Un programa que pida al usuario 4 números,
            los memorice (utilizando un array), calcule
            su media aritmética y después muestre en pantalla
            la media y los datos tecleados.*/


            double[] N = new double[4];  //Los Numeros 
            double S = 0;  //Suma
            double M;  //Medida

            Console.WriteLine("Bienvenido, este programa te ayudará a calcular la medida artmética de los 4 valores que desees");
            


            for (int i = 0; i < 4; i++)
            {
                Console.Write("Introduzca el valor #{0}: ", i + 1);
                N[i] = Convert.ToDouble(Console.ReadLine());
                S += N[i];
            }


            M = S / 4;
            Console.Write("\nLos numeros que introdujo son: ");
            for (int c = 0; c < 4; c++)


            {
                Console.Write(" ({0}) ", N[c]); 
            }
            Console.Write("\nResultado de la media aritmética: {0}", M); 

            Console.ReadKey();
        }
    }
}
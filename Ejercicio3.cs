﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3
{
    class Program
    {
        static void Main(string[] args)
        {
			/*Un programa que almacene en un array el número
			 de días que tiene cada mes (supondremos que es
			 un año no bisiesto), pida al usuario que le indique
			 un mes (1=enero, 12=diciembre) y muestre en pantalla
			 el número de días que tiene ese mes. */

			int[] dÍas = new int[3];
			int[] meses = new int[13];
			int Resultado = 1;

			Console.WriteLine("######Bienvenido, este programa te ayudara a saber la cantidad de días para cada mes del año######");
			Console.WriteLine("//////////////////////////////////////////////////////////////////////////////////////////////////\n");
			Console.Write("                 1 Enero                 7 Julio\n                 2 Febrero" +
				"               8 Agosto \n                 3 Marzo                 9 Septiembre\n                 4 Abril                " +
				"10 Octubre\n                 5 Mayo                 11 Noviembre\n                 6 Junio                12 Diciembre\n");
			Console.Write("Introduzca el numero del mes deseado: ");

			//Días
			dÍas[0] = 31;
			dÍas[1] = 30;
			dÍas[2] = 28;

			//Meses
			meses[1] = dÍas[0];
			meses[2] = dÍas[2];
			meses[3] = dÍas[0];
			meses[4] = dÍas[1];
			meses[5] = dÍas[0];
			meses[6] = dÍas[1];
			meses[7] = dÍas[0];
			meses[8] = dÍas[0];
			meses[9] = dÍas[1];
			meses[10] = dÍas[0];
			meses[11] = dÍas[1];
			meses[12] = dÍas[0]; 

			
			Resultado = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("El mes {0} cuenta con {1} días", Resultado, meses[Resultado]);

			Console.ReadKey();
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {

            /*Un programa que pida al usuario 5 números reales(pista:
             necesitarás un array de "float") y luego los muestre en
             el orden contrario al que se introdujeron.*/

            float[] numerosreales = new float[5];


            Console.WriteLine("Bienvenido, este programa le permite digitar 5 numeros y le regresa los 5 numeros en su orden contrario.");
            Console.WriteLine("\n_________________________________________________________________________________________________________");
            for (int i = 0; i < numerosreales.Length; i++)
            {
                Console.Write("Introduzca un valor: ");
                numerosreales[i] = Convert.ToInt32(Console.ReadLine());

            }
            Console.WriteLine("\n******************RESULTADO******************");
            int A = 4;
            while (A >= 0)
            {
                Console.Write(numerosreales[A] + ",   ");
                A--;
            }
            Console.ReadKey();

        }
    }
}

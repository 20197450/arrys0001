﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio4
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Un programa que pida al usuario
              10 números y luego calcule y muestre
              cuál es el mayor de todos ellos.*/

            int[] Nun = new int[10];
            int i, Mayor = 0, n = 0;

            Console.WriteLine("_________Bienvenido, este programa le dirá cual numero de los 10 es mayor._________ \n");
            Console.WriteLine("//////////////////////////////Introduzca 10 numeros//////////////////////////////");

            for (i = 0; i < 10; i++)
            {
                Console.Write("Introduzca dígito #{0}: ", i + 1);
                Nun[i] = Convert.ToInt32(Console.ReadLine());
            }
            while (n < 10)
            {
                if (Nun[n] > Mayor)
                {
                    Mayor = Nun[n];
                }
                n++;
            }
            Console.WriteLine("\nDe los numeros digitados, el mayor es: " + Mayor);



            Console.ReadKey();
        }
    }


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio8
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Crear un programa que defina un array de 5
          elementos de tipo float que representen las
          alturas de 5 personas.
          Obtener el promedio de las mismas.
        Contar cuántas personas son más altas que
        el promedio y cuántas más bajas.*/


            Console.WriteLine("Bienvenido, este programa te puede decir la cantidad de personas aon mas altas con la información que ingreses.");
            Console.WriteLine("////////////////////////////////////////////////////////////////////////////////////////////////////////////////");

            float[] Altura = new float[5];
            for (int i = 0; i < 5; i++)

            {Console.Write("Ingrese altura de la persona: ");
                Altura[i] = float.Parse(Console.ReadLine());
            }

            float SUMA = 0, P;
            for (int i = 0; i < 5; i++)


            {SUMA += Altura[i];
            }

            P = SUMA / 5;
            Console.WriteLine("\nPromedio: " + P);

            Console.WriteLine("///////////////////////////////////////////////////////////////////////////////////");

            int MAYOR = 0, MENOR = 0;
            for (int i = 0; i < 5; i++)

            {
                if (Altura[i] > P)
                {
                    MAYOR++;
                }
                if (Altura[i] < P)


                {MENOR++;
                }
            }
            Console.WriteLine("La cantidad de personas mas altas son: " + MAYOR);
            Console.WriteLine("La cantidad de personas mas bajas son: " + MENOR);
            Console.WriteLine("\nGRACIAS POR PREFERIRNOS");
            Console.ReadKey();
        }
    }
}